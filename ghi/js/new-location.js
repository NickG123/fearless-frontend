window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const element = document.getElementById('state');
        for (let state of data.states) {
            let option = document.createElement('option');
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            element.appendChild(option);


            // Set the '.value' property of the option element to the
            // state's abbreviation

            // Set the '.innerHTML' property of the option element to
            // the state's name

            // Append the option element as a child of the select tag
        }
    }
});
